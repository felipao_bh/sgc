<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Tour;
use Maatwebsite\Excel\Facades\Excel;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;



class ToursController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private  $tour;

    public function __construct(Tour $tour)
    {
        $this->middleware('auth');
        $this->tour = $tour;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = $this->tour->all();
        return view('admin.tours.index', compact('tours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create()
    {
        $tour = new \App\Tour();
        return view('admin.tours.create')->with('tour', $tour);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'personalities' => 'required',
            'profiles' => 'required',
            'price' => 'required',
        ]);

        $request->merge(array('personalities'=>implode(', ', $request->input('personalities'))));
        $request->merge(array('profiles'=>implode(', ', $request->input('profiles'))));
        $this->tour->create($request->all());
        return redirect()->route('admin.tours.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $tour = $this->tour->find($id);

        return view('admin.tours.edit', compact('tour'));
    }

    public function editar($id, Request $request)
    {
        $tour = $this->tour->find($id);
        $fileTour = \App\Tour::with('files')->find($id);


        JavaScript::put(['dados' => $tour]);

        return view('admin.tours.editar')->with('tour', $tour)->with('file', $fileTour);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $request->merge(array('personalities'=>implode(', ', $request->input('personalities'))));
        $request->merge(array('profiles'=>implode(', ', $request->input('profiles'))));
        $this->tour->find($id)->update($request->all());
        return redirect()->route('admin.tours.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->tour->find($id)->delete();
        return redirect()->route('admin.tours.index');
    }

    public function downloadExcel()
    {
        $tours = Tour::select('id', 'city', 'personalities', 'title', 'price', 'profiles', 'content', 'source', 'duration', 'chat')->get();
        Excel::create('tours', function($excel) use($tours) {
            $excel->sheet('Sheet 1', function($sheet) use($tours) {
                $sheet->fromArray($tours);
            });
        })->download('xls');
    }

    public function downloadCsv()
    {
        $tours = Tour::select('id', 'city', 'personalities', 'title', 'price', 'profiles', 'content', 'source', 'duration', 'chat')->get();
        Excel::create('tours', function($excel) use($tours) {
            $excel->sheet('Sheet 1', function($sheet) use($tours) {
                $sheet->fromArray($tours);
            });
        })->download('csv');
    }

    public function downloadPdf()
    {
        $tours = Tour::select('id', 'city', 'personalities', 'title', 'price', 'profiles', 'content', 'source', 'duration', 'chat')->get();
        Excel::create('tours', function($excel) use($tours) {
            $excel->sheet('Sheet 1', function($sheet) use($tours) {
                $sheet->fromArray($tours);
            });
        })->download('pdf');
    }

    public function uploadImage(Request $request)
    {

        $id = $request->input('id');

        $file = \Request::file('files');

        $fileName = $file->getClientOriginalName();

        $storagePath = storage_path().'/tours/imagens/';
        $url = $storagePath.$fileName;

        $fileModel = new \App\File();
        $fileModel->name = $fileName;
        $fileModel->url = $url;


        $tour = \app\Tour::find($id);
        $tour->files()->save($fileModel);

        $file->move($storagePath,$fileName);

        return redirect()->back()->with('upload-success', 'Arquivo enviado com sucesso');

    }

    public function destroyImage($tourId, $fileId)
    {
        $file = \App\File::find($fileId);

        $storagePath = storage_path().'/tours/imagens';

        $file->delete();

        unlink($storagePath.'/'.$file->name);

        return redirect()->back()->with('success', 'Arquivo removido com sucesso');
    }

    public function downloadImage($tourId, $fileId)
    {

        $file = \App\File::find($fileId);

        $storagePath = storage_path().'/tours/imagens';

        return \Response::download($storagePath.'/'.$file->name);

    }


}