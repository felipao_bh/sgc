<?php

namespace App\Http\Controllers;

use Countable;
use ArrayAccess;
use ArrayIterator;
use CachingIterator;
use JsonSerializable;
use IteratorAggregate;
use InvalidArgumentException;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Database\Eloquent\Collection;


class FileController extends Controller
{
    private $file;
    private $countFiles;
    private $collection;

    public function __construct(File $file)
    {
        $this->middleware('auth');
        $this->file = $file;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $file = \Request::file('files');
        $fileName = $file->getClientOriginalName();
        $storagePath = storage_path().'/tours/imagens/';
        $url = $storagePath.$fileName;
        $fileModel = new \App\File();
        $fileModel->name = $fileName;
        $fileModel->url = $url;

        $colecao = collect($fileModel);
        dd($colecao);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function collect($file)
    {
        $this->countFiles
        $collection = collect($file)->count()+1;
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($tourId, $fileId)
    {

        $file = \App\File::find($fileId);

        $storagePath = storage_path().'/tours/imagens';

        return \Response::download($storagePath.'/'.$file->name);

    }
}
