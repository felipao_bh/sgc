<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function tour()
    {
        return $this->belongsTo('app\Tour', 'tour_id');
    }
}
