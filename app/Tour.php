<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'city',
        'personalities',
        'title',
        'price',
        'profiles',
        'content',
        'source',
        'duration',
        'chat'
    ];

    public function files()
    {
        return $this->hasMany('App\File');

    }
}
