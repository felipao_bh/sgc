
<link rel="stylesheet" href="/bower_components/blueimp-file-upload/css/jquery.fileupload.css">

<!-- The fileinput-button span is used to style the file input field as button -->
<span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Selecionar arquivos...</span>
    <!-- The file input field used as target for the file upload widget -->
            <input id="fileupload" type="file" name="files">
        </span>
<br>
<br>
<!-- The global progress bar -->
<div id="progress" class="progress">
    <div class="progress-bar progress-bar-success"></div>
</div>

@if(Session::has('success'))
    <div class="alert alert-success">
        {!! Session::get('success') !!}
    </div>
@endif

<div class="alert alert-success hide" id="upload-success">
    Upload realizado com sucesso!
</div>

<table class="table table-bordered table-striped table-hover">
    <thead>
    <tr>
        <th>Nome</th>
        <th>Enviado Em</th>
        <th>Ações</th>
    </tr>
    </thead>
    <tbody>
        @foreach($tour->files as $file)
        <tr>
            <td>{!! $file->name !!}</td>
            <td>{!! $file->created_at !!}</td>
            <td>
                <a href="{!! route('admin.tours.image.download', [$tour->id,  $file->id]) !!}"
                   class="btn btn-xs btn-success">Download</a>

                <a href="{!! route('admin.tours.image.destroy', [$tour->id,  $file->id]) !!}"
                    class="btn btn-xs btn-danger" style="margin-top: 5px">Deletar</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>