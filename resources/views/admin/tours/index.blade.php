@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Tours</div>
                    <div class="panel-heading">
                        <a href="{{ url('admin/tours/downloads/excel') }}"><button class="btn btn-success" style="margin-bottom: 10px">Exportar XLS</button></a>
                        <a href="{{ url('admin/tours/downloads/csv') }}"><button class="btn btn-info" style="margin-bottom: 10px">Exportar CSV</button></a>
                        <a href="{{ url('admin/tours/downloads/pdf') }}"><button class="btn btn-danger" style="margin-bottom: 10px">Exportar PDF</button></a>

                        <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>CIDADE</th>
                                        <th>PERSONALIDADE</th>
                                        <th>TÍTULO</th>
                                        <th>PREÇO</th>
                                        <th>PERFIL(S)</th>
                                        <th>CONTEÚDO</th>
                                        <th>FONTE(URL)</th>
                                        <th>DURAÇÃO</th>
                                        <th>CHAT</th>
                                        <th>AÇÔES</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($tours as $tour)
                                    <tr>
                                        <td>{{ $tour-> id }}</td>
                                        <td>{{ $tour-> city }}</td>
                                        <td>{{ $tour-> personalities }}</td>
                                        <td>{{ $tour-> title }}</td>
                                        <td>{{ $tour-> price }}</td>
                                        <td>{{ $tour-> profiles }}</td>
                                        <td>{{ $tour-> content }}</td>
                                        <td>{{ $tour-> source }}</td>
                                        <td>{{ $tour-> duration }}</td>
                                        <td>{{ $tour-> chat }}</td>
                                        <td>
                                            <a href="{{ route('admin.tours.edit', ['id'=> $tour->id]) }}" class="btn btn-default">Editar</a>
                                            <a href="{{ route('admin.tours.destroy', ['id'=> $tour->id]) }}" class="btn btn-danger" style="margin-top: 5px">Deletar</a>
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>

@endsection