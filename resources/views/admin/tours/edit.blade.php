@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Tour</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/tours/update', [$tour->id])}}">
                            {{ csrf_field() }}


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">Cidade</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control" name="city" value="{{$tour->city}}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="personalities" class="col-md-4 control-label">Personalidade</label>

                                <div class="col-md-6">
                                    <!--<input id="personalities" type="text" class="form-control" name="personalities">-->

                                    <select class="selectpicker" id="personalities" multiple name="personalities[]" multiple title="Escolha a personalidade..." values="$tour->personalities[]">

                                        <option value="Agreeableness">Agreeableness</option>
                                        <option value="Conscientiousness">Conscientiousness</option>
                                        <option value="Extraversion">Extraversion</option>
                                        <option value="Emotional Range">Emotional Range</option>
                                        <option value="Openness">Openness</option>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Título</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{$tour->title}}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Preço</label>

                                <div class="col-md-6">
                                    <!--<input id="price" type="text" class="form-control" name="price">-->
                                    <select class="selectpicker"  title="Selecione a faixa de preço" name="price">
                                        <option value="1">Barato</option>
                                        <option value="2">Médio</option>
                                        <option value="3">Caro</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="profiles" class="col-md-4 control-label">Perfil(s)</label>

                                <div class="col-md-6">
                                    <!--<input id="profiles" type="text" class="form-control" name="profiles">-->

                                    <select class="selectpicker" multiple name="profiles[]" multiple title="Escolha o perfil...">
                                        <option value="beach & adventures">Beach & Adventures</option>
                                        <option value="cultural">Cultural</option>
                                        <option value="massive tours">Massive Tours</option>
                                        <option value="food">Food</option>
                                        <option value="partynight">Partynight</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="content" class="col-md-4 control-label">Conteúdo</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" input id="content" type="text" name="content">{{$tour->content}}</textarea>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="source" class="col-md-4 control-label">Fonte (URL)</label>

                                <div class="col-md-6">
                                    <input id="source" type="text" class="form-control" name="source" value="{{$tour->source}}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="duration" class="col-md-4 control-label">Duração</label>

                                <div class="col-md-6">
                                    <!--<input id="duration" type="text" class="form-control" name="duration">-->

                                    <select class="selectpicker"  title="Escolha a duração" name="duration">
                                        <option value="0.0208">30 Minutos</option>
                                        <option value="0.0312">45 Minutos</option>
                                        <option value="0.0417">1 Hora</option>
                                        <option value="0.0833">2 Horas</option>
                                        <option value="0.125">3 Horas</option>
                                        <option value="0.1667">4 Horas</option>
                                        <option value="0.2083">5 Horas</option>
                                        <option value="0.25">6 Horas</option>
                                        <option value="0.2917">7 Horas</option>
                                        <option value="0.3333">8 Horas</option>
                                        <option value="0.375">9 Horas</option>
                                        <option value="0.4167">10 Horas</option>
                                        <option value="1">1 dia</option>
                                        <option value="2">2 dias</option>
                                        <option value="3">3 dias</option>
                                        <option value="4">4 dias</option>
                                        <option value="5">5 dias</option>
                                        <option value="6">6 dias</option>
                                        <option value="7">7 dias</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="chat" class="col-md-4 control-label">Chat</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" input id="chat" type="text" name="chat">{{$tour->chat}}</textarea>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="images" class="col-md-4 control-label">Anexar imagens</label>

                                <!-- The fileinput-button span is used to style the file input field as button -->
                                <span class="btn btn-success fileinput-button" style="margin-left: 15px">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Selecionar arquivos...</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input id="fileupload" type="file" name="files[]" multiple>
                                </span>
                                <br>
                                <br>
                                <!-- The global progress bar -->
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Salvar
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection