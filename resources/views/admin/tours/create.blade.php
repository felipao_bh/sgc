@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Criar Novo Tour</div>
                    <div class="panel-body">

                        {!! Form::open(['route'=>['admin.tours.store'], 'method'=>'post', 'class'=>'form-horizontal']) !!}

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">Cidade</label>

                                <div class="col-md-6">
                                    <input id="searchTextField" type="text" class="form-control" name="city" placeholder="Selecione a cidade" autocomplete="on">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="personalities" class="col-md-4 control-label">Personalidade</label>

                                <div class="col-md-6">
                                    <!--<input id="personalities" type="text" class="form-control" name="personalities">-->

                                    <select class="selectpicker" multiple name="personalities[]" multiple title="Escolha a personalidade...">
                                        <option value="Agreeableness">Agreeableness</option>
                                        <option value="Conscientiousness">Conscientiousness</option>
                                        <option value="Extraversion">Extraversion</option>
                                        <option value="Emotional Range">Emotional Range</option>
                                        <option value="Openness">Openness</option>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Título</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">Preço</label>

                                <div class="col-md-6">
                                    <!--<input id="price" type="text" class="form-control" name="price">-->
                                        <select class="selectpicker"  title="Selecione a faixa de preço" name="price">
                                            <option value="1">Barato</option>
                                            <option value="2">Médio</option>
                                            <option value="3">Caro</option>
                                        </select>
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="profiles" class="col-md-4 control-label">Perfil(s)</label>

                                <div class="col-md-6">
                                <!--<input id="profiles" type="text" class="form-control" name="profiles">-->

                                    <select class="selectpicker" multiple name="profiles[]" multiple title="Escolha o perfil...">
                                        <option value="beach & adventures">Beach & Adventures</option>
                                        <option value="cultural">Cultural</option>
                                        <option value="massive tours">Massive Tours</option>
                                        <option value="food">Food</option>
                                        <option value="partynight">Partynight</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="content" class="col-md-4 control-label">Conteúdo</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" input id="content" type="text" name="content"></textarea>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="source" class="col-md-4 control-label">Fonte (URL)</label>

                                <div class="col-md-6">
                                    <input id="source" type="text" class="form-control" name="source"">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="duration" class="col-md-4 control-label">Duração</label>

                                <div class="col-md-6">
                                    <!--<input id="duration" type="text" class="form-control" name="duration">-->

                                    <select class="selectpicker"  title="Escolha a duração" name="duration">
                                        <option value="0.02">30 Minutos</option>
                                        <option value="0.03">45 Minutos</option>
                                        <option value="0.04">1 Hora</option>
                                        <option value="0.08">2 Horas</option>
                                        <option value="0.12">3 Horas</option>
                                        <option value="0.16">4 Horas</option>
                                        <option value="0.2">5 Horas</option>
                                        <option value="0.25">6 Horas</option>
                                        <option value="0.29">7 Horas</option>
                                        <option value="0.33">8 Horas</option>
                                        <option value="0.37">9 Horas</option>
                                        <option value="0.41">10 Horas</option>
                                        <option value="1">1 dia</option>
                                        <option value="2">2 dias</option>
                                        <option value="3">3 dias</option>
                                        <option value="4">4 dias</option>
                                        <option value="5">5 dias</option>
                                        <option value="6">6 dias</option>
                                        <option value="7">7 dias</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="chat" class="col-md-4 control-label">Chat</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" rows="3" input id="chat" type="text" name="chat"></textarea>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Salvar
                                    </button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection