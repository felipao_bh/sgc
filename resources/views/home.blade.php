@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Painel</div>

                <div class="panel-body">
                    Você está logado! <br> <br>

                    Para exibir todos os tours cadastrados, basta clicar no menu superior (Tours -> Listar)<br>
                    Para cadastrar novos Tours, clique em (Tours -> Criar Novo)

            </div>
        </div>
    </div>
</div>
@endsection
