<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('teste', function (){
    return "Olá mundo";
});

Route::get('admin/tours/teste', ['as' => 'admin.tours.teste', 'uses' => 'ToursController@teste']);
Route::get('admin/tours/index', ['as' => 'admin.tours.index', 'uses' => 'ToursController@index']);
Route::get('admin/tours/create', ['as' => 'admin.tours.create', 'uses' => 'ToursController@create']);
Route::post('admin/tours/store', ['as' => 'admin.tours.store', 'uses' => 'ToursController@store']);
Route::get('admin/tours/edit/{id}', ['as' => 'admin.tours.edit', 'uses' => 'ToursController@editar']);
Route::get('admin/tours/destroy/{id}', ['as' => 'admin.tours.destroy', 'uses' => 'ToursController@destroy']);
Route::any('admin/tours/update/{id}', ['as' => 'admin.tours.update', 'uses' => 'ToursController@update']);
Route::get('admin/tours/downloads/excel', ['as' => 'admin.tours.download.excel', 'uses' => 'ToursController@downloadExcel']);
Route::get('admin/tours/downloads/csv', ['as' => 'admin.tours.download.csv', 'uses' => 'ToursController@downloadCsv']);
Route::get('admin/tours/downloads/pdf', ['as' => 'admin.tours.download.pdf', 'uses' => 'ToursController@downloadPdf']);
Route::post('admin/tours/upload/image', ['as' => 'admin.tours.upload.image', 'uses' => 'ToursController@uploadImage']);
Route::get('admin/{tourId}/destroy/{fileId}', ['as' => 'admin.tours.image.destroy', 'uses' => 'ToursController@destroyImage']);
Route::get('admin/{tourId}/download/{fileId}', ['as' => 'admin.tours.image.download', 'uses' => 'ToursController@downloadImage']);


Auth::routes();

Route::get('/home', 'HomeController@index');
